package model

import (
	"crypto/tls"
	"github.com/jinzhu/gorm"
)

type Security int

const (
	SecurityNone Security = iota
	SecuritySTARTTLS
	SecurityTLS
)

type ProxyMailbox struct {
	gorm.Model
	Username               string  `gorm:"not null"`
	Address                string
	Security               Security
	Login                  string
	Password               string
	TLSConfig              *ProxyMailboxTLSConfig
	RemoteInboxMailboxName string
}

type ProxyMailboxTLSConfig struct {
	gorm.Model
}

func BuildTLSConfig(mailbox *ProxyMailbox) *tls.Config {
	return nil
}
