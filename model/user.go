package model

import "github.com/jinzhu/gorm"

type Credentials struct {
	gorm.Model
	Password string `json:"password", db:"password"`
	Username string `json:"username", db:"username"`
}
