module gitlab.com/ed13710/go-moderated-imap

go 1.13

require (
	github.com/jinzhu/gorm v1.9.12
	gitlab.com/ed13710/go-moderated-imap-proxy v0.0.0-20200325194701-79134aa3ae3c
)
