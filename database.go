package go_moderated_imap

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/ed13710/go-moderated-imap/model"
)

func InitDatabase(db *gorm.DB) {
	db.AutoMigrate(&model.ProxyMailbox{})
	db.AutoMigrate(&model.ProxyMailboxTLSConfig{})
	db.AutoMigrate(&model.Credentials{})

}
